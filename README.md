# My fossils

This is a repo of all of my [fossils](https://fossil-scm.org/).

## Submitting Tickets

You can submit a ticket here or in any of the other mirrors.

If you plan on being a regular contributor, then I can create an account for you on
[fossils.risto.codes](https://fossils.risto.codes), just submit a ticket or contact me and
tell me a little bit about yourself.

Mirrors:

- https://fossils.risto.codes (`fossil`)  
- https://gitlab.com/risto1/fossils (`git`)  
- https://git.sr.ht/~risto/fossils (`git`)  
- https://codeberg.org/risto/fossils (`git`)  


## Why Fossil?

Fossil is created by the same guy who wrote sqlite.

### Pros

Although I love `git`, I've moved away from it to prefer `fossil` for a variety of reasons:

- All code is a single fossil file, which makes it really easy to transport, encrypt, etc.  
- Easy to nest repos.  
- Immutable history.  
- All-in-one solution, with tickets, forum, wiki, tags, chat, accounts, etc. You choose what you need. Wiki and Markdown syntax support.  
- Customizable and scriptable.  
- Extremely easy to host, and since it's all-in-one it's truly decentralized. Any addons like deploy hooks can be Tcl scripts are kept along with the repo. Sensitive info like passwords are scrubbed on cloning.  
- No staging environment.  
- Patches are compatible with GNU patch.

### Cons

- Merging is similar to `git`, and uses the same sort of solution for tracking branches using tags, which are mutable. It has better search functionality than git tags though. Mercurial preserves branch history but doesn't have a lot of the other benefits.
